import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  // base_url = 'http://localhost:8000/api/v1'; // Api em Laravel

  base_url = 'http://localhost:8765/api/v1'; // Api em CakePhp

  constructor(private http: HttpClient) { }

  getInfo() {
    return this.http.get(`${this.base_url}/covid/info`);
  }

  getInfectedByRegion() {
    return this.http.get(`${this.base_url}/covid/infectedByRegion`);
  }
}
