<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Client;

/**
 * Covid Controller
 *
 * @method \App\Model\Entity\Covid[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CovidController extends AppController
{
    private function getData()
    {
        $http    = new Client();

        $content = $http->get('https://api.apify.com/v2/key-value-stores/TyToNta7jGKkpszMZ/records/LATEST?disableRedirect=true');

        return $content->getJson();
    }

    public function info()
    {
        $content   = $this->getData();

        $date_time = new \DateTime($content['lastUpdatedAtApify']);

        $date_time = $date_time->setTimeZone(new \DateTimeZone('America/Fortaleza'));

        $date_time = $date_time->format('d-m-Y') . ' ' . $date_time->format('H:m');

        $content   = [$date_time, $content['recovered'], $content['infected'],
            $content['deceased']];

        $this->set(compact('content'));
        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function infectedByRegion()
    {
        $content   = $this->getData();

        $content   = $content['infectedByRegion'];

        $this->set(compact('content'));
        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');
    }
}
