## VERSÕES

### PHP 7.2
### Cakephp 4
### Angular 7

## PASSOS PARA A EXECUÇÃO DO PROJETO

CakePhp Api com Angular

git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/projeto-cakephp-api-angular.git

Instalar a extensão intl do php, caso não tenha ela

Dentro de Cakephp-Api: 

composer install => instalar as dependências

Ao ser perguntado se deseja setar permissões as pastas, digite: Y

bin/cake server => rodar a aplicação via servidor do cakephp

Dentro de Angular-Consumindo-Api-Framework: 

npm install => instalar as dependências
ng serve => executar a aplicação angular